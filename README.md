### Source / Frameworks
* [CppMicroServices](https://github.com/CppMicroServices/CppMicroServices)
* [restbed](https://github.com/Corvusoft/restbed)
* [Restbed](https://github.com/Stiffstream/restinio)
* [Crow](https://github.com/ipkn/crow)
* [Pistache](http://pistache.io/)
* [eventuate](eventuate.io)
* [lagom](www.lagomframework.com)
* [Hipster Shop: Cloud-Native Microservices Demo Application](https://github.com/GoogleCloudPlatform/microservices-demo)
* [fabric8 - an end to end development platform](http://fabric8.io/)
 
### Portals
* [microservices.io](https://microservices.io/index.html)
* [List of Microservice Architecture related principles and technologies](https://github.com/mfornos/awesome-microservices)

### Articles
* [I’ve heard I can use any language for each microservice, is that correct?](https://medium.com/the-andela-way/ive-heard-i-can-use-any-language-for-each-microservice-is-that-correct-6bc63ca910db)
* [Modern C++ micro-service implementation + REST API](https://medium.com/audelabs/modern-c-micro-service-implementation-rest-api-b499ffeaf898)
* [High-Performance Microservices with C/C++ /pdf/](https://www.maximilianhaupt.com/2016-07-20%20C++%20Microservices.pdf)
* [How to choose a database for your microservices](https://www.infoworld.com/article/3236291/how-to-choose-a-database-for-your-microservices.html)
* [Selecting the Right Database for Your Microservices](https://thenewstack.io/selecting-the-right-database-for-your-microservices/)
* [PL / Kubernetes – słownik pojęć](https://blog.gutek.pl/2018/02/26/kubernetes-slownik-pojec/)
* [Microservices with Spring Boot - Part 1 - Getting Started](http://www.springboottutorial.com/creating-microservices-with-spring-boot-part-1-getting-started)

##### HTTP API's 
* [Understanding RPC Vs REST For HTTP APIs](https://www.smashingmagazine.com/2016/09/understanding-rest-and-rpc-for-http-apis/)
* [RPC Style vs. REST Web APIs](https://blog.jscrambler.com/rpc-style-vs-rest-web-apis/)
 
### Demos
* [Microservices Demos](https://ewolff.com/microservices-demos.html)
* [Microservice Consul Sample](https://github.com/ewolff/microservice-consul)
* [Microservices-demo](https://github.com/harishkadamudi/microservices-demo)
* [spring-boot-microservice-demo](https://github.com/onerbal/spring-boot-microservice-demo)
* [microservices-demo](https://github.com/paulc4/microservices-demo) --> article [Microservices with Spring](https://dzone.com/articles/microservices-with-spring)
* [Sock Shop](https://microservices-demo.github.io/)